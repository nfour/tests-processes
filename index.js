import { forkFile } from 'process-communication'

(async () => {
    console.log('[[[index]]]')
    const args = [
        'test',
        {
            test: 1
        }
    ]

    const job = forkFile('./forkMe')
    const result = await job.request('execute', args)

    job.kill()

    console.log('[[[index]]] results')
    console.log({result})
})()
